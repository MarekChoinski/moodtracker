from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator

class CustomUserManager(BaseUserManager):
    def create_user(self, login, password= None):#,_name,_surname,_city):
        if not login:# or not _name or not _surname or not _city:
            raise ValueError("Users must have a login!")

        user = self.model(
            login=login
        )
        # user.name = _name
        # user.surname=_surname
        # user.city=_city
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password):
        user = self.create_user(
            login=login,
            password=password
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class CityManager(models.Manager):

    def create(self,_name):
        city = self.model(name=_name)
        city.save()

class DoctorManager(models.Manager):
    
    def create(self,_name,_surname,_city,_tel):

        doc = self.model(name=_name,surname=_surname,tel=_tel)
        doc.city = _city
        doc.save()

class MoodManager(models.Manager):

    def create(self,_mood,_user,_date):

        moodobj = self.model(mood=_mood,user=_user,date=_date)
        moodobj.save()
        


class City(models.Model):
    name = models.TextField(primary_key=True)

    objects = CityManager()

    def __str__(self):
        return self.name


class CustomUser(AbstractBaseUser):

    login = models.CharField(max_length = 20, primary_key = True)
    password = models.CharField(max_length = 20)
    name = models.CharField(max_length = 20)
    surname = models.CharField(max_length = 20)
    city = models.ForeignKey(City,on_delete=models.CASCADE, null = True)


    # Required for the AbstractBaseUser class
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name ='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['password']


    objects = CustomUserManager()

    def __str__(self):
        return self.login

    # Required for the AbstractBaseUser class
    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

class Mood(models.Model):

    id = models.AutoField(primary_key = True)
    mood = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    date = models.DateField(auto_now=True)

    objects = MoodManager()

class Doctor(models.Model):
    name = models.CharField(max_length = 20)
    surname = models.CharField(max_length = 20)
    city = models.ForeignKey(City,on_delete=models.CASCADE)
    tel = models.IntegerField()

    objects = DoctorManager()

    def __str__(self):
        return self.name


