from rest_framework import serializers
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta():
        model = CustomUser
        fields  = ('login','name','surname','city')

class MoodSerializer(serializers.ModelSerializer):
    class Meta():
        model = Mood
        fields = ('mood','user','date')

class DoctorSerializer(serializers.ModelSerializer):
    class Meta():
        model = Doctor
        fields = ('name','surname','city','tel')