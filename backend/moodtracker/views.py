from django.shortcuts import render
from .serializers import *
from .models import *
from rest_framework import viewsets
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.views import View
from rest_framework.mixins import CreateModelMixin
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from django.views.decorators.csrf import csrf_exempt


class AllUsersView(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class AuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'login'   : user.login
        })

class UserMoods(viewsets.ViewSet):


    def list(self, request, pk=None):
        print(request.GET.get("login"))
        queryset = Mood.objects.filter(user__login = request.GET.get("login"))
        serializer = MoodSerializer(queryset, many=True)
        return Response(serializer.data)

    

class DoctorsInCity(viewsets.ViewSet):


    def list(self, request, pk=None):
        print(request.GET.get("city"))
        city = request.data['city']
        queryset = Doctor.objects.filter(city__name = city)
        serializer = DoctorSerializer(queryset, many=True)
        return Response(serializer.data)
# @csrf_exempt 
class AddUser(APIView):

    # queryset = CustomUser.objects.all()
    # serializer_class = UserSerializer

    def post(self, request, format=None):

        data = request.data
        print(data)
        _login = data['login']
        _password = data['password']
        _name = data['name']
        _surname = data['surname']

        try:
            _city = City.objects.get(name = data['city'])
        except City.DoesNotExist:
            return Response("This city doesnt exist in DB. Correct type error or add it to DB first", status.HTTP_400_BAD_REQUEST)

        try:
            user = CustomUser.objects.create_user(login=_login, password=_password)#,name=_name,surname=_surname,city=_city)
            user.name = _name
            user.surname = _surname
            user.city =_city
            user.save()
            return Response("Mareń 201 response",status = status.HTTP_201_CREATED)
        except :
            return Response("Cos zrobiles zle, Zrob wszystko jak nalezy bo nie chce mi się pisac info przy obsłudze błędów", status.HTTP_400_BAD_REQUEST)
        


class AddCity(APIView):

    def post(self, request, format=None):

        try:
            City.objects.create(request.data['name'])
            return Response("Mareń 201 response",status = status.HTTP_201_CREATED)
        except: return Response("Cos zrobiles zle, Zrob wszystko jak nalezy bo nie chce mi się pisac info przy obsłudze błędów", status.HTTP_400_BAD_REQUEST)


class AddDoctor(APIView):

    def post(self, request, format=None):
        

        try:
            data = request.data
            city = City.objects.get(name = data['city'])
            name = data['name']
            surname = data['surname']
            tel = data['tel']
            Doctor.objects.create(name,surname,city,tel)
            return Response("Mareń 201 response",status = status.HTTP_201_CREATED)
        except:
            return Response("Cos zrobiles zle, Zrob wszystko jak nalezy bo nie chce mi się pisac info przy obsłudze błędów", status.HTTP_400_BAD_REQUEST)
            



class AddMood(APIView):

    def post(self, request, format=None):

        data = request.data
        _login = data['login']
        _mood = data['mood']
        _date = data['date']

        _user = CustomUser.objects.get(login=_login)
        Mood.objects.create(_mood,_user,_date)
        return Response("Mareń 201 response",status = status.HTTP_201_CREATED)
