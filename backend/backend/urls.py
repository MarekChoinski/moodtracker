"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from moodtracker.views import AuthToken
from moodtracker.views import UserMoods, AllUsersView, DoctorsInCity, AddUser,AddCity, AddDoctor, AddMood
from rest_framework import routers


router = routers.DefaultRouter()
router.register('user_moods', UserMoods, basename = '/')
router.register('all_users',AllUsersView)
router.register('doctors_in_city',DoctorsInCity,basename='/')
#router.register('add_user',AddUser,basename='/')



urlpatterns = [
    path('',include(router.urls)),
    path('admin/', admin.site.urls),
    # path('',include('moodtracker.urls')),
    path('api-token/',AuthToken.as_view(), name = 'merek xD'),
    # path('user_moods/',UserMoods.as_view(), name = 'mareń')
    path('add_user/',AddUser.as_view(), name = 'nikola wroc'),
    path('add_city/',AddCity.as_view(), name = 'nikola wroc'),
    path('add_doctor/',AddDoctor.as_view(), name = 'nikola wroc'),
    path('add_mood/',AddMood.as_view(), name = 'nikola wroc'),
]
