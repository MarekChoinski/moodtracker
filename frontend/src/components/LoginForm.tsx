import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, Redirect } from 'react-router-dom';
import Logo from '../assets/logo.png';
import axios from 'axios';

const LoginForm: React.FC = () => {

    const [logged, setLogged] = useState<boolean>(false);

    const { register, handleSubmit } = useForm();
    const onSubmit = (data: any) => {
        console.log(data);
        axios.post('/api-token/', {
            username: data.login,
            password: data.password
          })
          .then(function (response:any) {
              console.log(response);
              
            localStorage.setItem('token', response!.data!.token);
            setLogged(true);
          })
          .catch(function (error:any) {
            console.log(error);
          });
    }

    return (
        logged?
        <Redirect exact to="/"/>:
        <>
            <img src={Logo} alt="Logo" className="logo" />
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="form"
            >
                <label
                    htmlFor="login"
                    className="form__label"
                >
                    Login
            </label>
                <input
                    name="login"
                    ref={register}
                    className="form__input"
                />
                <label
                    htmlFor="login"
                    className="form__label"
                >
                    Hasło
            </label>
                <input
                    name="password"
                    ref={register}
                    className="form__input"
                />
                <button
                    type="submit"
                    className="form__button"
                >
                    ZALOGUJ SIĘ
            </button>
                <Link to="/signup" className="signupLink">
                    Nie masz konta? Zarejestruj się!
                </Link>
            </form>
        </>
    );
}

export default LoginForm;