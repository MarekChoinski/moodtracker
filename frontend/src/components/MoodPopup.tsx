import React from 'react';
import { IMoodDay } from '../interfaces/MoodDay';

interface Prop extends IMoodDay {
    onExit: () => any,
}

const MoodPopup: React.FC<Prop> = props => {

    const {
        mood,
        date,
        description,
        onExit
    } = props;

    return (
        <div className="moodPopup">
            <div
                className="moodPopup__exit"
                onClick={onExit}
            >
                X
            </div>
            <p className="moodPopup__mood">
                Samopoczucie: <span>{mood}</span>
            </p>
            <p className="moodPopup__date">
                <span>Data: </span> {date.toLocaleDateString()}
            </p>

            <hr />

            <p className="moodPopup__description">
                {description} Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam enim voluptate beatae, quis, officia voluptatibus molestias veritatis nesciunt dolore quia odio excepturi laborum tempora id, mollitia nobis adipisci nihil sequi.
            </p>
        </div>
    );
}

export default MoodPopup;