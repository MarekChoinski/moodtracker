import React from 'react';
import { useForm } from 'react-hook-form';
import Logo from '../assets/logo.png';

const SingUpForm: React.FC = () => {

    const { register, handleSubmit } = useForm();
    const onSubmit = (data: any) => console.log(data);

    return (
        <>
            <img src={Logo} alt="Logo" className="logo" />
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="form"
            >
                <label
                    htmlFor="login"
                    className="form__label"
                >
                    Login
            </label>
                <input
                    name="login"
                    ref={register}
                    className="form__input"
                />
                <label
                    htmlFor="firstname"
                    className="form__label"
                >
                    Imię
            </label>
                <input
                    name="firstname"
                    ref={register}
                    className="form__input"
                />
                <label
                    htmlFor="login"
                    className="form__label"
                >
                    Hasło
            </label>
                <input
                    name="password"
                    ref={register}
                    className="form__input"
                />
                <button
                    type="submit"
                    className="form__button"
                >
                    ZAREJESTRUJ SIĘ
            </button>
            </form>
        </>
    )
}

export default SingUpForm;