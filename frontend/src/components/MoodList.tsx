import React, { useEffect, useState } from 'react';
import { IMoodDay } from '../interfaces/MoodDay';
import { generateRandomMoodDay, getFixedDay } from '../utils';
import MoodPopup from './MoodPopup';

interface Prop {
    randomMoods: IMoodDay[]
}

const MoodList: React.FC<Prop> = props => {

    const {
        randomMoods
    } = props;

    const [shownMood, setShownMood] = useState<IMoodDay | null>();
    const [parsedMoods, setParsedMoods] = useState<(IMoodDay | null)[]>([]);

    useEffect(() => {
console.log(randomMoods);


        let parsed: (IMoodDay | null)[] = [];

        const firstDay = getFixedDay(randomMoods[0].date);

        // prepend with empty
        if (firstDay !== 0) { // not when sunday
            for (let i = 0; i < firstDay - 1; i++) {
                parsed.push(null);
                console.log(null);
            }
        }

        // add empty cells
        randomMoods.forEach((mood: IMoodDay, i: number) => {
            // first simply push
            if (i === 0) {
                parsed.push(mood);
            }
            // well added
            else if (
                (getFixedDay(randomMoods[i - 1].date) + 1 === getFixedDay(randomMoods[i].date)) ||
                // last day was monday
                (getFixedDay(randomMoods[i - 1].date) - 1 === 6 && getFixedDay(randomMoods[i].date) === 1)
            ) {
                parsed.push(mood);
            }
            else {
                let num_of_nulls = 0;
                if (getFixedDay(randomMoods[i - 1].date) === 7)
                    num_of_nulls = getFixedDay(randomMoods[i].date) - 1;
                else
                    num_of_nulls = getFixedDay(randomMoods[i].date) - (getFixedDay(randomMoods[i - 1].date) + 1);

                if (num_of_nulls < 0)
                    num_of_nulls = 7 + num_of_nulls;

                for (let i = 0; i < num_of_nulls; i++) {
                    parsed.push(null);
                    console.log(null);
                }
                parsed.push(mood);
            }
        })

        // TODO
        parsed = [...parsed]

        setParsedMoods(parsed);
    }, [randomMoods]);

    return (
        <section
            className="moodList"
        >
            <h3
                className="moodList__headerWrapper"
            >
                {[
                    "PON",
                    "WTO",
                    "ŚRO",
                    "CZW",
                    "PIA",
                    "SOB",
                    "NDZ"
                ].map(day => {
                    return (
                        <span
                            className="moodList__header"
                        >
                            {day}
                        </span>
                    )
                })}
            </h3>

            <ul
                className="moodList__list"
            >
                {
                    parsedMoods.length ?
                        parsedMoods.map(moodDay => {
                            return (
                                moodDay ?
                                    <li
                                        key={moodDay!.date.getTime()}
                                        className={`moodList__day moodList__day--level_${moodDay.mood}`}
                                        onClick={() => setShownMood({
                                            ...moodDay
                                        })}
                                    /> :

                                    <li
                                        className="moodList__day moodList__day--empty"
                                    />
                            );
                        }) :
                        null
                }
            </ul>
            {
                shownMood ?
                    <MoodPopup
                        mood={shownMood.mood}
                        description={shownMood.description}
                        date={shownMood.date}
                        onExit={() => setShownMood(null)}
                    />
                    :
                    null
            }

        </section>
    );
}

export default MoodList;