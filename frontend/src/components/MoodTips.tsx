import React from 'react';
import { useForm } from 'react-hook-form';
interface Prop {
    avg: number
}

const MoodTips: React.FC<Prop> = props => {

    const {
        avg
    } = props;

    return (
        <div className="moodTips" style={{lineHeight: "1.5"}}>
            <span className="moodTips__average">
                Średnie samopoczucie: {avg}
            </span>

            <p className="moodTips__description">
                Twoje samopoczucie jest średnie. W celu poprawy nastroju mozna udac sie na spracer lub pobiegac. Warto tez wyjsc do ludzi i sie uśmiechnąć. :)
            </p>

        </div>
    );
}

export default MoodTips;