import React, { useState } from 'react';
import { useForm } from 'react-hook-form';

interface Prop {
    onSubmitF: any
}

const MoodForm: React.FC<Prop> = props => {

    const [hid, setHid] = useState<boolean>(false);

    const {
        onSubmitF
    } = props;

    const mood = [
        "Wolałbym umrzeć w tej chwili",
        "Nie mogę wytrzymać smutku",
        "Czuję się fatalnie",
        "Jest mi bardzo smutno",
        "Kiepsko ze mną",
        "Jest neutralnie",
        "Jest ok",
        "Jest Dobrze",
        "Mam wspaniały humor",
        "Czuję ekstazę zycia :)",
    ]

    const { register, handleSubmit } = useForm();
    const onSubmit = (data: any) => {
        data = {
            ...data,
            createData: Date.now(),
        }
        console.log(data);
        onSubmitF(data);
        setHid(true);
    }

    return (
        <form
            onSubmit={handleSubmit(onSubmit)}
            className="form form--mood"
            style={{display: hid? "none": "flex"}}
        >
            <label
                htmlFor="mood"
                className="form__label"
            >
                Jak się czujesz? (1-10)
            </label>
            <select
                name="mood"
                ref={register}
                className="form__select"
            >
                {
                    mood.map((m: string, i: number) => {
                        return (
                            <option
                                value={i + 1}
                                key={m}
                            >
                                {i + 1} - {m}
                            </option>
                        )
                    })
                }
            </select>

            <label
                htmlFor="description"
                className="form__label"
            >
                Dodaj krótki opis
            </label>
            <textarea
                name="description"
                rows={4}
                cols={50}
                ref={register}
                placeholder="Opisz krótko swoje samopoczucie w dzisiejszym dniu..."
                className="form__textarea"
            />

            <button
                type="submit"
                className="form__button"
            >
                Wyślij
            </button>
        </form>
    );
}

export default MoodForm;