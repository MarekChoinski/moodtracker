import { IMoodDay } from "./interfaces/MoodDay";

export const generateRandomMoodDay = (amount: number): IMoodDay[] => {
    let moodDays: IMoodDay[] = [];

    let firstDate: Date = new Date();
    firstDate.setDate(firstDate.getDate() 
    // + Math.floor(Math.random() * (7 - 1 + 1)) + 1
    );
    firstDate.setDate(firstDate.getDate() - amount);

    let dateIterator = 0;
    for (let i = 0; i < amount; i++, dateIterator++) {

        let date = new Date();
        date.setDate(firstDate.getDate() + dateIterator);

        if (Math.floor(Math.random() * (3 - 1 + 1)) + 1 === 1) // 33% chance
        {
            date.setDate(date.getDate() + 1);
            dateIterator++;
        }

        moodDays.push({
            mood: Math.floor(Math.random() * (10 - 1 + 1)) + 1, // mood 1-10
            description: "Some random description",
            date: date
        });
    }

    return moodDays;
}

//monday is 1, sunday is 7
export const getFixedDay = ((date: Date) =>
    date.getDay() !== 0 ?
        date.getDay() : 7
)