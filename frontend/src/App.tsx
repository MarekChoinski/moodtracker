import React from 'react';
import './styles/index.scss';
import SignUpForm from './components/SignupForm';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // Link
} from "react-router-dom";
import LoginForm from './components/LoginForm';
import MainPage from './pages/MainPage';


const App: React.FC = () => {
  return (
    <Router>
      <main>
        <Switch>
          <Route exact path="/" component={MainPage} />
          <Route exact path="/signup" component={SignUpForm} />
          <Route exact path="/login" component={LoginForm} />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
