import React, { useEffect, useState } from 'react';
import MoodForm from '../components/MoodForm';
import MoodList from '../components/MoodList';
import MoodTips from '../components/MoodTips';
import Logo from '../assets/logo.png';
import { Redirect } from 'react-router';
import { generateRandomMoodDay } from '../utils';
import { IMoodDay } from '../interfaces/MoodDay';

const gen_randomMoods = generateRandomMoodDay(25);

let gavg = 0;

gen_randomMoods.forEach(m=>{
    gavg += m.mood;
})

// for (let index = 24, j =1; index >=0; index--, j++) {
//     let firstDate: Date = new Date();
//     gen_randomMoods[index].date.setDate(firstDate.getDate() - j)
    
// }

gavg = gavg/25;

const MainPage: React.FC = () => {

    const [randomMoods, setRandomMoods]= useState(gen_randomMoods);
    const [avg, setAvg]= useState(gavg);


    const onSubmitF = (data:any) => {

// console.log(randomMoods[24].date.getDate());


        let newMood:IMoodDay = {
            mood: Number(data.mood),
            date: new Date(randomMoods[24].date.getTime() + (60 * 60 * 24 * 1000)),
            description: data.description
        };

        let x = [...randomMoods, newMood]
        setRandomMoods(x);

        console.log(randomMoods);
        

        let navg = 0;

        [...randomMoods, newMood].forEach(m=>{
            navg += m.mood;
        });

        setAvg(Math.round((navg/26) * 100) / 100);

        console.log(data);
        console.log(navg);
        console.log(newMood);
        
        
    }

    return (
        localStorage.token?
        <>
            <img src={Logo} alt="Logo" className="logo" />
            <MoodList randomMoods={randomMoods}/>
            <MoodTips avg={avg}/>
            <MoodForm onSubmitF={onSubmitF}/>
        </>:
        <Redirect to="/login"/>
    )
}

export default MainPage;