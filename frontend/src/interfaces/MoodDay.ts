export interface IMoodDay {
    mood: number,
    date: Date,
    description: string
}